#!/usr/bin/perl
use strict;
use Data::Dumper;
use Bio::SeqIO;
use List::MoreUtils qw(uniq);
use Data::Dumper qw(Dumper);
use Getopt::Long qw(GetOptions);

# perl gff2table.pl --gff INPUT.gff > OUTPUT.tabgff

my $gff;

GetOptions(
    'gff=s' => \$gff
) or die "Usage: $0 --gff PATH > PATHTOOUTPUT\n";

# my $gff=$gffpath;
# my @criteria = ("eC_number","gene","ID","inference","locus_tag","Name","Parent","product","protein_id");

my %TAB;
my %TAB2;
my @crit;

print STDERR 'Reading ' . $gff . "\n\t--Define features criteria\n";
open(GFF,$gff) ||  die "File not found.\n";
while(<GFF>){
  chomp;
  if($_=~"\tCDS\t"){ #$_=~"^gnl" &
    # print $_ . "\n";
    my @line=split("\t",$_);
    my $feat = $line[8];
    my @feat2 = split(";",$feat);
    my @id = split("\=", $feat2[0]);
    # print $id[1] . "\n";
      foreach my $i (@feat2){
        # print $i . "\n";
            my @feat3 = split("\=", $i);
                push @crit, @feat3[0];
      }
  }
}
my @uniq_crit = uniq @crit;
print STDERR "Criteria: @uniq_crit\n";

my @headermisc = ('seqname', 'source', 'feature', 'start', 'end', 'score', 'strand', 'frame');
print join ("\t", @headermisc, @uniq_crit) . "\n";


print STDERR 'Reading ' . $gff . "\n\t--Parse CDS criteria.\n";
open(GFF,$gff) ||  die "File not found.\n";
while(<GFF>){
  chomp;
  if($_=~"\tCDS\t"){   # prefixe $_=~"^gnl" &
    # print $_ . "\n";
    my @line=split("\t",$_);
    my $feat = $line[8];
    my $misc = join("\t",@line[0..7]);
    # print $misc . "\n";
    my @feat2 = split(";",$feat);
    my @id = split("\=", $feat2[0]);
    # print $id[1] . "\n";
      foreach my $i (@feat2){
        # print $i . "\n";
        foreach my $j (@uniq_crit){
          if($i =~ "$j="){
            my @feat3 = split("\=", $i);
            $TAB{$id[1]}{$j}=$feat3[1];
            # print $feat3[0] . "\n";
          }
        }
      }
  # print Dumper(\%TAB{$id[1]});
  my @featF;
    foreach my $j (@uniq_crit){
    push @featF, $TAB{$id[1]}{$j};
    }
  print join ("\t",$misc, @featF) . "\n";

 # print $TAB{$id[1]}{ID} . "\t" . $TAB{$id[1]}{eC_number} . "\t" . $TAB{$id[1]}{gene} . "\t" . $TAB{$id[1]}{Name} . "\t" . $TAB{$id[1]}{locus_tag} . "\t" . $TAB{$id[1]}{inference} . "\t" . $TAB{$id[1]}{protein_id} . "\t" . $TAB{$id[1]}{product} ."\n";
 # print join('\t', $TAB{id[1]});
    # print $feat2[0] . "\t" . $feat2[1] . "\n";
    # print $id[1] . "\n";

  }

}


# my %hash = (4 => "model1", 2 => "model2");
# print Dumper(\%hash);
# my $str = join(", ", map { "$_ X $hash{$_}" } keys %hash);
# print $str ."\n";




#gff2table.pl --gff 21.gff | cut -f1 -d=|sort -u # pour connaitre tous les criteria

# eC_number
# gene
# ID
# inference
# locus_tag
# Name
# Parent
# product
# protein_id

# diffrna
Several scripts for RNAseq data statistical analysis and more.

## gff2table
Apply transformation to obtain exhaustive informations of raw gff in tabulated format.
```
perl gff2table.pl --gff INPUT.gff > OUTPUT.tabgff
```

## rnaseq_DA_functions.R
Functions for statistical analysis of RNAseq counts tables + gene enrichment analysis
* diff_deseq2: deseq2 analysis on specific conditions
* diff_edgeR: edgeR analysis on specific conditions
* output_diff: agglomerate all differential analysis results, output synthetic table of significant features with annotations.
* output_all: output all results of differential analysis with annotation
topGo_enrich: return results of enrichment analysis with topgo.


## topgo_analysis.R
Executable Rscript allowing to perform topgo GO term enrichment analysis.
```
Rscript ./topgo_analysis.R -a all_genes.gomap -b diff_genes.gomap
```
